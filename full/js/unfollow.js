if (typeof once === "undefined") {
	var once = true;

	chrome.storage.local.get('inputsValue', function(storage) {
		limit = parseInt(storage.inputsValue[8], 10);
		sessionUnfollow = Number.MAX_SAFE_INTEGER;

		if (!storage.inputsValue[10]) {
			sessionUnfollow = parseInt(storage.inputsValue[9], 10);
		}

		sleepTimeMin = minToSec(parseInt(storage.inputsValue[11]), 10);
        sleepTimeMax = minToSec(parseInt(storage.inputsValue[12]), 10);
		unfollowEveryone = storage.inputsValue[13];

		if (storage.inputsValue[14]) {
			daysBeforeUnfollow = parseInt(storage.inputsValue[15], 10);
			smartUnfollow = true;
		}

        chrome.storage.local.get('userId', function(storage) {
            userId = parseInt(storage.userId, 10);
            doAsync();
        });
    });
}