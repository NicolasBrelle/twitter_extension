async function isAccountBlocked() {
    await sleep(getRandom(1, 2));
    let ahref = document.querySelectorAll("a[href]");
    let supportLinkCount = 0;

    for (let i = 0; i < ahref.length; i++) {
        if (ahref[i].getAttribute("href").includes(errorURI)) {
            supportLinkCount++;
        }

        if (supportLinkCount == 2) {
            console.log("Action restricted by Twitter.");
            shouldStop = true;
            return;
        }
    }
}

async function getUsersCell() {
    let primaryColumn = document.querySelector('[data-testid="primaryColumn"]');
    let btns = primaryColumn.querySelectorAll("[data-testid]");
    let usersCell = Array.from(btns).filter(btn => {
        return btn.getAttribute('data-testid').includes('UserCell');
    });

    return usersCell;
}

async function canFollowUser(userCell) {
    let userBtn = userCell.querySelectorAll("[role='button']");

    if (userBtn.length < 1) {
        return false;
    }

    userBtn = userBtn[0];
    return (userBtn.getAttribute('data-testid').includes('follow') && !userBtn.getAttribute('data-testid').includes('unfollow'));
}

async function getUsersLink(usersCell) {
    let linkList = [];

    for (let i = 0; i < usersCell.length; i++) {
        let link = usersCell[i].querySelector("a[href]");

        if (link.length == 0) {
            linkList.push("");
            continue;
        }
        linkList.push(link.href);
    }
    return linkList;
}

async function fillAllUsers(usersCell) {
    let linkList = await getUsersLink(usersCell);
    let lastUser = allUsers[allUsers.length - 1];
    let addToUsers = (allUsers.length == 0);
    let alreadyFollowedCount = 0;

    for (let i = 0; i < linkList.length; i++) {
        if (addToUsers) {
            if (!await canFollowUser(usersCell[i])) {
                alreadyFollowedCount++;
                continue;
            }
            allUsers.push(linkList[i]);
        }

        if (linkList[i] == lastUser) {
            addToUsers = true;
        }
    }
    return alreadyFollowedCount;
}

async function fillTrashList(usersCell) {
    let linkList = await getUsersLink(usersCell);
    let lastUser = trashList[trashList.length - 1];
    let addToUsers = (trashList.length == 0);

    for (let i = 0; i < linkList.length; i++) {
        if (addToUsers) {
            trashList.push(linkList[i]);
        }

        if (linkList[i] == lastUser) {
            addToUsers = true;
        }
    }
}

async function likeAndRetweetMostFollowers(index) {
    let userLink = usersCell[index].querySelector('a[href]');
    let likeWindow = window.open(userLink, "_blank", "location=no,height=570,width=520,scrollbars=no,status=no");
    var goAhead = null;

    likeWindow.addEventListener("DOMContentLoaded", () => {
        clearInterval(timeout);
        goAhead = true;
    });

    var timeout = setInterval(() => {
        goAhead = false;
        clearInterval(timeout);
    }, 6000);

    await sleep(getRandom(2, 3));
    window.focus();

    while (goAhead === null) {
        await sleep(1);
    }

    if (!goAhead) {
        likeWindow.close();
        return;
    }

    for (let i = 0; i < likeAndRetweetCount; i++) {
        let likeBtn = await getSpecificBtn(likeWindow, 'like');

        if (!likeBtn) {
            break;
        }

        likeBtn.click();
        let retweetBtn = await getSpecificBtn(likeWindow, 'retweet');

        if (!retweetBtn) {
            break;
        }

        await sleep(getRandom(2, 4));
        retweetBtn.click();

        await sleep(getRandom(1, 2));
        let confirmBtn = await getSpecificBtn(likeWindow, 'retweetConfirm');

        if (!confirmBtn) {
            break;
        }
        confirmBtn.click();
        await sleep(getRandom(1, 4));
    }
    likeWindow.close();
}

async function getInfosAndFollow(userLink) {
    let followWindow = window.open(userLink, "_blank", "location=no,height=570,width=520,scrollbars=no,status=no");
    let followBtns = [];
    let usernames = [];
    let allInfos = [];
    var goAhead = null;

    followWindow.addEventListener("DOMContentLoaded", () => {
        clearInterval(timeout);
        goAhead = true;
    });

    var timeout = setInterval(() => {
        goAhead = false;
        clearInterval(timeout);
    }, 6000);

    await sleep(getRandom(2, 3));
    window.focus();

    while (goAhead === null) {
        await sleep(1);
    }

    if (!goAhead) {
        followWindow.close();
        return 0;
    }

    let spans = followWindow.document.querySelectorAll("span");
    let btns = followWindow.document.querySelectorAll("[data-testid]");

    usernames = Array.from(spans).filter((span) => {
        return span.innerText[0] == "@" && span.innerText != myName;
    });

    allInfos = Array.from(spans).filter((span) => {
        return !isNaN(parseInt(span.innerText, 10));
    });

    followBtns = Array.from(btns).filter((btn) => {
        return btn.getAttribute("data-testid").includes("follow");
    });

    if (!followBtns[0] || !followBtns[0].getAttribute("data-testid") || followBtns[0].getAttribute("data-testid").includes("unfollow")) {
        followWindow.close();
        return 0;
    }

    usersInfo.push([allInfos[0].innerText, allInfos[2].innerText, usernames[0].innerText]);
    await sleep(getRandom(2, 4));
    followBtns[0].click();
    setCookie("followed", parseInt(getCookie("followed"), 10) + 1, getCookie("expireFollow"));
    await sleep(getRandom(2, 4));
    followWindow.close();
    return 1;
}

async function followCycle() {
    let followed = 0;
    let notFollowedCount = 0;
    let usersCell = [];
    let notFollowedLength = allUsers.length - savedIndex;

    document.getElementById('statusCircle').style.backgroundColor = processingColor;
    document.getElementById('statusText').innerText = processingText + followed + '/' + sessionFollow;

    usersCell = await getUsersCell();
    if (!usersCell.length) {
        shouldStop = true;
        return;
    }

    while (notFollowedLength < sessionFollow) {
        let prevAllSize = allUsers.length;
        let prevTrashListLength = trashList.length;

        await sleep(getRandom(4, 7));
        usersCell = await getUsersCell();
        alreadyFollowedCount += await fillAllUsers(usersCell);
        notFollowedLength = allUsers.length - savedIndex - alreadyFollowedCount;
        await fillTrashList(usersCell);

        let documentHeight = document.querySelector("div[data-at-shortcutkeys]").clientHeight;
        window.scrollTo({
            top: window.scrollY + documentHeight,
            behavior: "smooth",
        });

        if (prevAllSize == allUsers.length && prevTrashListLength == trashList.length) {
            break;
        }
    }

    console.log(allUsers);

    while (followed < sessionFollow && !shouldStop) {
        let newFollow = await getInfosAndFollow(allUsers[savedIndex + followed + notFollowedCount]);

        followed += newFollow;
        notFollowedCount -= newFollow - 1;

        if (!shouldStop) {
            document.getElementById('statusCircle').style.backgroundColor = processingColor;
            document.getElementById('statusText').innerText = processingText + followed + '/' + sessionFollow;
            await isAccountBlocked();

            if (getCookie('followed') >= limit) {
                shouldStop = true;
                return followed;
            }

            if (allUsers.length - savedIndex - followed - notFollowedCount == 0) {
                break;
            }

            if (window.checkLikeUs !== undefined) {
                await checkLikeUs();
            }
        }
        await sleep(getRandom(3, 6));
    }
    savedIndex += followed;
    return followed;
}

function getMostFollowers() {
    let mostFollowers = -1;
    let indexMostFollowers = 0;

    for (let i = 0; i < usersInfo.length; i++) {
        if (usersInfo[i][1] && parseInt(usersInfo[i][1].replace(/,/i, ''), 10) > mostFollowers) {
            mostFollowers = parseInt(usersInfo[i][1].replace(/,/i, ''), 10);
            indexMostFollowers = i;
        }
    }
    return indexMostFollowers;
}

async function main() {
    let sleepTime = 0;
    let statusParent = document.querySelector('header[role="banner"]');
    statusParent.innerHTML += statusPopup;

    document.querySelector('div[data-at-shortcutkeys]').style += ';pointer-events: none;';
    window.scrollTo({
        top: 0,
        behavior: 'smooth'
    });

    while (!shouldStop) {
        let followed = await followCycle();
        let date = 'Twitter' + getFormattedDate(getCookie("expire"));
        let prevUsersInfo = (localStorage.getItem(date)) ? localStorage.getItem(date).split(',') : [];

        for (let i = 0; i < usersInfo.length; i++) {
            prevUsersInfo.push(usersInfo[i][2]);
        }
        localStorage.removeItem(date);
        localStorage.setItem(date, prevUsersInfo);

        doRequest('POST', '/actions/ajaxSaveAction', {user_id: userId, action: 'follow', service : 'twitter', count: followed}, () => {
            var data = JSON.parse(request.responseText);
            console.log(data);
        });

        if (shouldStop) {
            break;
        }

        stopMethod = stopNotRunning;

        if (likeAndRetweetCount != 0) {
            let indexMostFollowers = getMostFollowers();

            await likeAndRetweetMostFollowers(indexMostFollowers);
            await sleep(getRandom(1, 2));
            if (!document.getElementById('statusCircle')) {
                statusParent = document.querySelector('header[role="banner"]');
                statusParent.innerHTML += statusPopup;
            }
        }

        sessionFollow = Math.round(getRandom(followBeforeSleepMin, followBeforeSleepMax));
        sleepTime = Math.ceil(getRandom(sleepTimeMin, sleepTimeMax));

        document.getElementById('statusCircle').style.backgroundColor = sleepColor;
        while (sleepTime > 0) {
            if (sleepTime < 60) {
                document.getElementById('statusText').innerText = sleepText + sleepTime + 's';
            }
            else {
                document.getElementById('statusText').innerText = sleepText + Math.ceil(secToMin(sleepTime)) + 'min';
            }
            await sleep(1);
            sleepTime--;
        }
    }

    console.log('Process finished; Total followed: ' + getCookie('followed'));
    document.getElementById('statusCircle').style.backgroundColor = finishColor;
    document.getElementById('statusText').innerText = finishText;
    await sleep(getRandom(1, 2));
    location.reload();
}

function doAsync() {
    if (!getCookie('followed')) {
        setFollowCookie(1);
    }

    console.log('Followed today : ', getCookie('followed'));
    if (getCookie('followed') < limit && sessionFollow > 0) {
        main();
    }
}

function getFormattedDate(date) {
    let monthArray = ['Jan', 'Feb', 'Mar', 'Apr', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    let splitedDate = date.split(/ /g);
    let month = (monthArray.indexOf(splitedDate[2]) + 1 >= 10) ? monthArray.indexOf(splitedDate[2]) + 1 : '0' + monthArray.indexOf(splitedDate[2]) + 1;

    return month + '/' + splitedDate[1] + '/' + splitedDate[3];
}

function getCookie(cname)  {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');

    for (var i = 0; i <ca.length; i++) {
        var c = ca[i];

        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function setCookie(cname, cvalue, date)  {
    var expires = "expires="+ date;

    document.cookie = cname + "=" + cvalue + ";" + expires + ";domain=twitter.com;path=/";
}

function setFollowCookie(exdays) {
    var date = new Date();

    date.setTime(date.getTime() + (exdays*24*60*60*1000));
    if (getCookie("expireFollow") == "") {
        setCookie("expireFollow", date.toUTCString(), date.toUTCString());
    }
    setCookie("followed", 0, date.toUTCString());
}

function doRequest(method, URI, data, callback) {
    var formData = new FormData();

    request.open(method, baseUrl + URI);
    request.onload = callback;

    if (data != null) {
        for (let key in data) {
            formData.append(key, data[key]);
        }
        request.send(formData);
        return;
    }
    request.send();
}

function stopRunning() {
    if (document.getElementById('statusCircle') !== null && document.getElementById('statusCircle').length != 0) {
        document.getElementById('statusCircle').style.backgroundColor = stoppingColor;
        document.getElementById('statusText').innerText = stoppingText;
    }
    forceStop = true;
}

function stopNotRunning() {
    if (document.getElementById('statusCircle') !== null && document.getElementById('statusCircle').length != 0) {
        document.getElementById('statusCircle').style.backgroundColor = stoppingColor;
        document.getElementById('statusText').innerText = stoppingText;
    }
    location.reload();
}

if (once_global === undefined) {
    var once_global = true;

	var request = new XMLHttpRequest();
	var baseUrl = "https://extension-connect.isoluce.net";
    var link = "https://twitter.com/GHFR_by_ETGR";
    var errorURI = 'support.twitter.com';
    var followBeforeLike = 50;
    var userId = 0;

    var minToSec = (min) => (min * 60);
    var secToMin = (sec) => (sec / 60);
    var getRandom = (min, max) => (Math.random() * (max - min) + min);
    var sleep = (seconds) => new Promise(_ => setTimeout(_, seconds * 1000));

    var limit = 0;
    var followBeforeSleepMin = 0;
    var followBeforeSleepMax = 0;
    var sleepTimeMin = 0;
    var sleepTimeMax = 0;
    var likeAndRetweetCount = 0;
    var myName = "";

	var stopMethod = stopNotRunning;
    var alreadyFollowedCount = 0;
    var shouldStop = false;
    var sessionFollow = 0;
    var savedIndex = 0;
    var usersInfo = [];
    var allUsers = [];
    var trashList = [];

    var processingText = "Follow : ";
    var processingColor = "orange";
    var sleepText = "Sleep : ";
    var sleepColor = "red";
    var stoppingText = "Stopping...";
    var stoppingColor = "red";
    var stoppingText = "Stopping...";
    var stoppingColor = "red";
    var finishText = "Finish !";
    var finishColor = "green";

    var statusPopup =
    '<style>\
        .margin-top{margin-top:2px;}.margin-left{margin-left:10px;}.status_text{font-weight: bold;font-size: 16px;display: inline-block;text-align: center;}.circle{display:inline-block;width:18px;height:18px;border-radius:18px;vertical-align:baseline;vertical-align:middle;}\
    </style>\
    <div style="background-color: rgba(0, 0, 0, 0.4);left:0;right:0;top:0;bottom:0;position:fixed;"></div>\
    <div style="bottom: 10px;right: 10px;width:200px;position: fixed;z-index: 151;background-color: #ffff;border: rgb(207, 217, 222) 1px solid;border-radius: 16px;padding: 10px;"><div style="display: flex;flex-direction: column;margin-bottom: 10px;"><div style="border-bottom: 1px solid rgba(var(--b6a,219,219,219),1);flex-direction: col;height: 43px;-webkit-flex-direction: row;">\
    <h1 class="m82CD  TNiR1" style="overflow: hidden;text-overflow:ellipsis;white-space:nowrap;display:flex;flex-grow:1;font-size:16px;font-weight: 600; justify-content: center;line-height: 24px;text-align: center;"><p id="statusCircle" class="circle margin-left margin-top" style="background-color:' + processingColor + ';margin-right:10px;"></p>Extension</h1>\
    </div></div><span style="font-size:20px;margin-left:20px;margin-right:20px;font-family: inherit;font-weight:bold;display:block;">\
    <p id="statusText" class="status_text margin-left margin-top"> Status ' + processingText + '</p>\
    </span></div></div>';

    setInterval(() => {
        let checking = setInterval(() => {
            chrome.runtime.sendMessage({directive: "throttling"});
        }, 500);

        requestAnimationFrame(() => {
            clearInterval(checking);
        });
    }, 100);

    chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
        if (request.stop === undefined && request.isRunning === undefined) {
            return;
        }
        if (request.stop) {
            stopMethod();
            sendResponse({});
            return;
        }
        sendResponse({running: true});
    });
}