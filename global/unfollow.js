async function isAccountBlocked() {
    await sleep(getRandom(1, 2));
    let ahref = document.querySelectorAll("a[href]");
    let supportLinkCount = 0;

    for (let i = 0; i < ahref.length; i++) {
        if (ahref[i].getAttribute("href").includes(errorURI)) {
            supportLinkCount++;
        }

        if (supportLinkCount == 2) {
            console.log("Action restricted by Twitter.");
            shouldStop = true;
            return;
        }
    }
}

async function isFollowingYou(userCell) {
    if (unfollowEveryone) {
        return true;
    }

    let spans = userCell.querySelectorAll('span');
    let followYouSpans = Array.from(spans).filter(span => {
        return (span.textContent === lang[currentLang].followsYouText);
    });

    return (followYouSpans.length != 0);
}

async function canUnfollow(userCell) {
    let userBtn = userCell.querySelectorAll("[role='button']");

    if (userBtn.length < 1) {
        return false;
    }

    userBtn = userBtn[0];
    return userBtn.getAttribute('data-testid').includes('unfollow');
}

async function getUsersCell() {
    let primaryColumn = document.querySelector('[data-testid="primaryColumn"]');
    let btns = primaryColumn.querySelectorAll("[data-testid]");
    let usersCell = Array.from(btns).filter(btn => {
        return btn.getAttribute('data-testid').includes('UserCell');
    });

    return usersCell;
}

async function getUsersLink(usersCell) {
    let linkList = [];

    for (let i = 0; i < usersCell.length; i++) {
        let link = usersCell[i].querySelector("a[href]");

        if (link.length == 0) {
            linkList.push("");
            continue;
        }
        linkList.push(link.href);
    }
    return linkList;
}

async function fillAllUsers(usersCell) {
    let linkList = await getUsersLink(usersCell);
    let lastUser = allUsers[allUsers.length - 1];
    let addToUsers = (allUsers.length == 0);
    let skippedUser = 0;

    for (let i = 0; i < linkList.length; i++) {
        if (addToUsers) {
            if (await isFollowingYou(usersCell[i]) || !await canUnfollow(usersCell[i])) {
                skippedUser++;
                continue;
            }
            allUsers.push(linkList[i]);
        }

        if (linkList[i] == lastUser) {
            addToUsers = true;
        }
    }
    return skippedUser;
}

async function fillTrashList(usersCell) {
    let linkList = await getUsersLink(usersCell);
    let lastUser = trashList[trashList.length - 1];
    let addToUsers = (trashList.length == 0);

    for (let i = 0; i < linkList.length; i++) {
        if (addToUsers) {
            trashList.push(linkList[i]);
        }

        if (linkList[i] == lastUser) {
            addToUsers = true;
        }
    }
}

async function unfollowUser(userLink) {
    let unfollowWindow = window.open(userLink, "_blank", "location=no,height=570,width=520,scrollbars=no,status=no");
    let goAhead = null;

    unfollowWindow.addEventListener("DOMContentLoaded", () => {
        clearInterval(timeout);
        goAhead = true;
    });

    var timeout = setInterval(() => {
        goAhead = false;
        clearInterval(timeout);
    }, 6000);

    while (goAhead === null) {
        await sleep(1);
    }

    if (!goAhead) {
        unfollowWindow.close();
        return 0;
    }

    let btns = unfollowWindow.document.querySelectorAll("[data-testid]");
    unfollowBtns = Array.from(btns).filter((btn) => {
        return btn.getAttribute("data-testid").includes("unfollow");
    });

    if (!unfollowBtns[0] || !unfollowBtns[0].getAttribute("data-testid")) {
        unfollowWindow.close();
        return 0;
    }

    await sleep(getRandom(2, 4));
    unfollowBtns[0].click();
    await sleep(getRandom(2, 3));

    let confirmUnfollowBtn = unfollowWindow.document.querySelector("[data-testid=confirmationSheetConfirm]");
    if (confirmUnfollowBtn == null || confirmUnfollowBtn.length == 0) {
        unfollowWindow.close();
        return 0;
    }

    confirmUnfollowBtn.click();
    setCookie("unfollowed", parseInt(getCookie("unfollowed"), 10) + 1, getCookie("expireUnfollow"));
    await sleep(getRandom(2, 5));
    unfollowWindow.close();
    return 1;
}

async function unfollowCycle() {
    let unfollowed = 0;
    let notUnfollowedCount = 0;
    let usersCell = [];
    let notUnfollowedLength = allUsers.length - savedIndex;

    document.getElementById('statusCircle').style.backgroundColor = processingColor;
    document.getElementById('statusText').innerText = processingText + unfollowed + '/' + sessionUnfollow;

    usersCell = await getUsersCell();
    if (!usersCell.length) {
        shouldStop = true;
        return;
    }

    while (notUnfollowedLength < sessionUnfollow) {
        let prevAllSize = allUsers.length;
        let prevTrashListLength = trashList.length;

        await sleep(getRandom(4, 7));
        usersCell = await getUsersCell();
        doNotUnfollowCount += await fillAllUsers(usersCell);
        notUnfollowedLength = allUsers.length - savedIndex - doNotUnfollowCount;
        await fillTrashList(usersCell);

        let documentHeight = document.querySelector("div[data-at-shortcutkeys]").clientHeight;
        window.scrollTo({
            top: window.scrollY + documentHeight,
            behavior: "smooth",
        });

        if (prevAllSize == allUsers.length && prevTrashListLength == trashList.length) {
            break;
        }
    }

    while (unfollowed < sessionUnfollow && !shouldStop) {
        let newUnfollow = await unfollowUser(allUsers[savedIndex + unfollowed + notUnfollowedCount]);

        unfollowed += newUnfollow;
        notUnfollowedCount -= newUnfollow - 1;

        if (!shouldStop) {
            document.getElementById('statusCircle').style.backgroundColor = processingColor;
            document.getElementById('statusText').innerText = processingText + unfollowed + '/' + sessionUnfollow;
            await isAccountBlocked();

            if (getCookie('unfollowed') >= limit) {
                shouldStop = true;
                return unfollowed;
            }

            if (allUsers.length - savedIndex - unfollowed - notUnfollowedCount == 0) {
                break;
            }
        }
        await sleep(getRandom(3, 6));
    }
    savedIndex += unfollowed;
    return unfollowed;
}

async function main() {
    let sleepTime = 0;
    let statusParent = document.querySelector('header[role="banner"]');
    statusParent.innerHTML += statusPopup;

    document.querySelector('div[data-at-shortcutkeys]').style += ';pointer-events: none;';
    window.scrollTo({
        top: 0,
        behavior: 'smooth'
    });

    await initSkipUser();

    while (!shouldStop) {
       let unfollowed = await unfollowCycle();

        doRequest('POST', '/actions/ajaxSaveAction', {user_id: userId, action: 'unfollow', service : 'twitter', count: unfollowed}, () => {
            var data = JSON.parse(request.responseText);
            console.log(data);
        });

        if (shouldStop) {
            break;
        }

        stopMethod = stopNotRunning;
        sleepTime = Math.ceil(getRandom(sleepTimeMin, sleepTimeMax));
        document.getElementById('statusCircle').style.backgroundColor = sleepColor;
        while (sleepTime > 0) {
            if (sleepTime < 60) {
                document.getElementById('statusText').innerText = sleepText + sleepTime + 's';
            }
            else {
                document.getElementById('statusText').innerText = sleepText + Math.ceil(secToMin(sleepTime)) + 'min';
            }
            await sleep(1);
            sleepTime--;
        }
    }
}

function doAsync() {
    if (!getCookie('unfollowed')) {
        setUnfollowCookie(1);
    }

    console.log('Unfollowed today : ', getCookie('unfollowed'));
    if (getCookie('unfollowed') < limit && sessionUnfollow > 0) {
        main();
    }
}

async function initSkipUser() {
    let dateDaysAgo = new Date();
	let usersInfo = getAllStorage();
	let usersDate = Object.keys(usersInfo);

    if (!smartUnfollow || usersDate.length <= 0) {
        return;
    }

    dateDaysAgo.setDate(dateDaysAgo.getDate() - daysBeforeUnfollow);
    dateDaysAgo = getFormattedDate(dateDaysAgo.toUTCString());
    dateDaysAgo = dateToInt(dateDaysAgo);

    for (let i = 0; i < usersDate.length; i++) {
        if (dateToInt(usersDate[i]) <= dateDaysAgo) {
            localStorage.removeItem(usersDate[i]);
            continue;
        }

        let splitedUsers = usersInfo[usersDate[i]].split(',');
        for (let j = 0; j < splitedUsers.length; j++) {
            if (splitedUsers[j][0] == '@') {
                splitedUsers[j] = splitedUsers[j].substring(1, splitedUsers[j].length)
            }
            usersToSkip.push(splitedUsers[j].toLowerCase());
        }
    }
}

function doRequest(method, URI, data, callback) {
    var formData = new FormData();

    request.open(method, baseUrl + URI);
    request.onload = callback;

    if (data != null) {
        for (let key in data) {
            formData.append(key, data[key]);
        }
        request.send(formData);
        return;
    }
    request.send();
}

function getFormattedDate(date) {
    let monthArray = ['Jan', 'Feb', 'Mar', 'Apr', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    let splitedDate = date.split(/ /g);
    let month = (monthArray.indexOf(splitedDate[2]) + 1 >= 10) ? monthArray.indexOf(splitedDate[2]) + 1 : '0' + monthArray.indexOf(splitedDate[2]) + 1;
    return month + '/' + splitedDate[1] + '/' + splitedDate[3];
}

function dateToInt(date) {
	let splitedDate = date.split('/');

	return splitedDate[2] + splitedDate[0] + splitedDate[1];
}

function getAllStorage() {
    let values = {};
    let keys = Object.keys(localStorage);
	let i = keys.length;
	let isDate = new RegExp("Twitter../../....");

    while (i--) {
		if (isDate.test(keys[i])) {
			values[keys[i].replace('Twitter', '')] = localStorage.getItem(keys[i]);
		}
    }
    return values;
}

function getCookie(cname)  {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');

    for (var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function setCookie(cname, cvalue, date)  {
    var expires = "expires="+ date;
    document.cookie = cname + "=" + cvalue + ";" + expires + ";domain=twitter.com;path=/";
}

function setUnfollowCookie(exdays) {
    var date = new Date();
    date.setTime(date.getTime() + (exdays*24*60*60*1000));
    if (getCookie("expireUnfollow") == "") {
        setCookie("expireUnfollow", date.toUTCString(), date.toUTCString());
    }
    setCookie("unfollowed", '0', date.toUTCString());
}

function stopRunning() {
    if (document.getElementById('statusCircle') !== null && document.getElementById('statusCircle').length != 0) {
        document.getElementById('statusCircle').style.backgroundColor = stoppingColor;
        document.getElementById('statusText').innerText = stoppingText;
    }
    forceStop = true;
}

function stopNotRunning() {
    if (document.getElementById('statusCircle') !== null && document.getElementById('statusCircle').length != 0) {
        document.getElementById('statusCircle').style.backgroundColor = stoppingColor;
        document.getElementById('statusText').innerText = stoppingText;
    }
    location.reload();
}

if (once_global === undefined) {
    var once_global = true;

	var currentLang = document.querySelector('html[lang]').getAttribute('lang');
	var baseUrl = "https://extension-connect.isoluce.net";
    var errorURI = "support.twitter.com";
	var request = new XMLHttpRequest();

	var stopMethod = stopNotRunning;
    var doNotUnfollowCount = 0;
	var shouldStop = false;
	var usersToSkip = [];
    var savedIndex = 0;
    var trashList = [];
    var allUsers = [];
	var userId = -1;

	var unfollowEveryone = false;
	var daysBeforeUnfollow = 0;
	var smartUnfollow = false;
	var sessionUnfollow = 0;
	var sleepTimeMin = 0;
	var sleepTimeMax = 0;
	var limit = 0;

	var lang = {
		en: {
			followsYouText: "Follows you",
			followingButtonText: "Following",
			confirmationButtonText: "Unfollow"
		},
		es: {
			followsYouText: "Te sigue",
			followingButtonText: "Siguiendo",
			confirmationButtonText: "Dejar de seguir"
		},
		fr: {
			followsYouText: "Abonné",
			followingButtonText: "Abonné",
			confirmationButtonText: "Se désabonner"
		}
	};

    var minToSec = (min) => (min * 60);
    var secToMin = (sec) => (sec / 60);
    var getRandom = (min, max) => (Math.random() * (max - min) + min);
    var sleep = (seconds) => new Promise(_ => setTimeout(_, seconds * 1000));

    var processingText = 'Unfollow : ';
    var processingColor = 'orange';
    var sleepText = 'Sleep : ';
    var sleepColor = 'red';
    var stoppingText = "Stopping...";
    var stoppingColor = "red";
    var finishText = 'Finish !';
    var finishColor = 'green';

    var statusPopup =
    '<style>\
        .margin-top{margin-top:2px;}.margin-left{margin-left:10px;}.status_text{font-weight: bold;font-size: 16px;display: inline-block;text-align: center;}.circle{display:inline-block;width:18px;height:18px;border-radius:18px;vertical-align:baseline;vertical-align:middle;}\
    </style>\
    <div style="background-color: rgba(0, 0, 0, 0.4);left:0;right:0;top:0;bottom:0;position:fixed;"></div>\
    <div style="bottom: 10px;right: 10px;width:200px;position: fixed;z-index: 151;background-color: #ffff;border: rgb(207, 217, 222) 1px solid;border-radius: 16px;padding: 10px;"><div style="display: flex;flex-direction: column;margin-bottom: 10px;"><div style="border-bottom: 1px solid rgba(var(--b6a,219,219,219),1);flex-direction: col;height: 43px;-webkit-flex-direction: row;">\
    <h1 class="m82CD  TNiR1" style="overflow: hidden;text-overflow:ellipsis;white-space:nowrap;display:flex;flex-grow:1;font-size:16px;font-weight: 600; justify-content: center;line-height: 24px;text-align: center;"><p id="statusCircle" class="circle margin-left margin-top" style="background-color:' + processingColor + ';margin-right:10px;"></p>Extension</h1>\
    </div></div><span style="font-size:20px;margin-left:20px;margin-right:20px;font-family: inherit;font-weight:bold;display:block;">\
    <p id="statusText" class="status_text margin-left margin-top"> Status ' + processingText + '</p>\
    </span></div></div>';

    setInterval(() => {
        let checking = setInterval(() => {
            chrome.runtime.sendMessage({directive: "throttling"});
        }, 500);

        requestAnimationFrame(() => {
            clearInterval(checking);
        });
    }, 100);

    chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
        if (request.stop === undefined && request.isRunning === undefined) {
            return;
        }
        if (request.stop) {
            stopMethod();
            sendResponse({});
            return;
        }
        sendResponse({running: true});
    });
}